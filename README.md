# Advanced Lane Finding Project

#### The goals / steps of this project are the following:

- Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
- Apply a distortion correction to raw images.
- Use color transforms, gradients, etc., to create a thresholded binary image.
- Apply a perspective transform to rectify binary image ("birds-eye view").
- Detect lane pixels and fit to find the lane boundary.
- Determine the curvature of the lane and vehicle position with respect to center.
- Warp the detected lane boundaries back onto the original image.
- Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

---

## Camera Calibration

The code for this step is given in first code cell of solution.ipynb. I have used provided 9x6 chessboard images to find the corners and their corresponding location coordinates using **cv2.findChessboardCorners** method. `objp` is replicated coordinates of 9x6 chessboard and `corners` is x,y coordinates of corners of each image. We get corner locations for each chessboard image taken with our camera. Afterwards, we use aggreate **objpoints** and **imgpoints** corresponding to ideal and actual corner coordinates of provided images, to calculate camera distortion coefficients and camera matrix using **cv2.calibrateCamera** method. Finally **cal_undistort** method takes in an image and returns undistorted copy of the input using calculated distortion coefficients and camera matrix.  Here are sample images and their undistorted versions:

![Undistorted Chessboard](./output_images/undistorted_chessboard.png)

![Undistorted Traffic](./output_images/undistorted_traffic.png)

---

## Gradient and Color thresholding

Sobel operator is kind of gradient that we can apply to grayscale image in x and y direction. This gives us edges in the image similar to Canny egde detection algorithm but with finer control. I have implemented sobel gradient detection and filtering in **abs_sobel_thresh** method in code cell with title '3- Sobel Gradient and other thresholding methods'. Additionally I have tried variations for application of sobel gradient, such as taking absolute magnitude of gradients in both x and y direction, or thresholding by direction of gradient computed by taking of **arctangent** of y gradient divided by x gradient. Examples of thresholding methods applied are given:


![Gradient Thresholding](./output_images/gradient_thresholding.png)




As we can observe applying gradient thresholding in X direction or combining X and Y gradient yield cleanest result.
To explore posibilities of binary thresholding even further, I decided to try color thresholding using HSV and HLS color schemes. To demonstrate why doing might be useful , I have plotted image with sepated H,S,L,V components: 


![Color Filtering](./output_images/color_filter.png)


The S component showed good contrast in region with lane lines, so I decided to try binary thresholding only using S component of the image. This method is implemented in **color_thresh** method in code cell 3. The resulting thresholded binary image is provided as follows:


![S Binary Thresholding](./output_images/s_binary.png)




The resulting image had fine separation of yellow lane, and experiments showed that white lane separation was problematic. Also, there was still some noise around lanes that was registered by applied filters. Hence I decided to apply perspective tranform before figuring out final binary thresholding methods.

---

## Perspective Transform

The implementation for perspective transform is given in code cell titled '4-Perspective tranform'. I have implemented method called **warp_image** that takes in image, src and dst point locations used for perspective tranformation. Within **warp_image** method called **cv2.getPerspectiveTransform** uses src and dst arrays to get perspective transormation matrix. This matrix is used in **cv2.warpPerspective** method to tranform the perspective of the input image. 
After some experimentation src and dst points were found as follows:


```
    ht_window = np.uint(img_size[0]/1.55)
    hb_window = np.uint(img_size[0])
    c_window = np.uint(img_size[1]/2)
    ctl_window = c_window - .145*np.uint(img_size[1]/2)
    ctr_window = c_window + .145*np.uint(img_size[1]/2)
    cbl_window = c_window - .95*np.uint(img_size[1]/2)
    cbr_window = c_window + .95*np.uint(img_size[1]/2)

    src = np.float32([[cbl_window,hb_window],[cbr_window,hb_window],[ctr_window,ht_window],[ctl_window,ht_window]])

    dst = np.float32([[cbl_window,img_size[0]],[cbr_window,img_size[0]],
                      [cbr_window,0],[cbl_window,0]])
```



which corresponded to following coordinates:


| Source        | Destination   | 
|:-------------:|:-------------:| 
| 32, 720       | 320, 0        | 
| 1248, 720     | 1248, 720     |
| 732, 464      | 1248, 0       |
| 547, 464      | 32, 0         |


I have verified that parameters above are satisfactory by checking that lane lines in transformed image are more or less parallel as shown below:

![Original Warped](./output_images/original_warped.png)

After  getting warped image I was ready to implement my final binary thresholding method.


--- 


## Binary Pipeline - Putting it all together


To reduce the noise that might get captured while detecting lane lines, I have decided to use warped image instead of original image. To extract the relevant shades of yellow and white colors, I have implemented method called **color_mask** that uses thresholding of separate color elements via **cv2.inRange**. This method uses all 3 elements of  HSV(Hue, Saturation, Value) color schema to precisely extract shades of yellow and shades of white that are of interest for lane line detection. Experimentation yielded following 8bit threshold values:

```
yellow_hsv_low  = np.array([ 0,  75,  100])
yellow_hsv_high = np.array([ 80, 255, 255])

white_hsv_low  = np.array([ 0,   0,   230])
white_hsv_high = np.array([ 255,  35, 255])
```
To get even better results, I have combined color-thresholding with Sobel X gradient binary thresholding and got following results:


![Color and Gradient Thresholding](./output_images/color_grad_thresholding.png)


All the final steps of perspective tranformation, yellow and white color masking and combining with sobel gradient were put together in method called **binary_pipeline** provided in code cell 7.


---

## Detecting lane pixels.

The implementation of lane pixels detection is provided in code cell titled '8- Sliding Window Polyfit'.
Here I have implemented method provided in lectures. **sliding_window_polyfit** method searches for pixels  around lane line area of the input image, which  is warped and binary thresholded. Initial location for searching is found using histogram peaks of binary pixels. Sample histogram is shown below:

![Sample Histogram](./output_images/histogram.png)
 
 
Histogram is calculated using bottom half of the input image. The two peaks correspond to left and right lanes, giving us starting location for our lane pixel search. We iterate the searching procedure with window height of around  **80 pixels** (720px /9 windows), and width of approximately **200px** (-+100px margin). With each iteration we find  x and y coordinates of pixels that we think correspond to lane line pixels. After finding all the relevant pixels we fit those to the second order polynomial with **np.polyfit** method and get polynomial coefficients for the left and right lanes. After calculating polynomial coefficients we can approximate the lane to polynomial curve as demostrated:

![Lane Detected](./output_images/poly_fit.png)

In order to able to reuse already calculated lane line polynomial coefficients and not to make blind sliding_window search in each iteration, I have implemented method called **polyfit_using_prev_fit**, which is provided in lectures. This method takes in binary warped image, already fitted lane line coefficients and calculates updated lane line polynomial coefficients corresponding to lane line that is within margin of input lane line. 


---

## Caclulating  the radius of curvature of the lanes and offset of vehicle center from lane center

Implementation of calculation of curve radius  and position of vehicle with respect to lane center is given code cell 10- Calculating curve radius and center distance in method called **curverad_and_center_dist**.

In order to calculate real world curvetaure radiuses, first we define pixels space to meters  coefficients in x and y directions  as follows:
```
    ym_per_pix = 30/720 # meters per pixel in y dimension
    xm_per_pix = 3.7/700 # meters per pixel in x dimension
    
```
Then we calculate new polynomial coefficients in real world space as follows.  

$$Rcuve = [1+ (dx/dy)^{2})]^{3/2}/|d^{2}x/dy^{2}|$$ 

which gives us following equation after tranformation detailed in lecture notes.

$$ Rcurve = (1+(2Ay+B)^{2})^{3/2}/(2A) $$

Here A and B correspond to first and second coefficients of newly fitted second degree polynomial. After coding the equation above we get curvature radius.

To calculate the position of vehicle wrt. lane center we assume that pixel world position of center of the car is center of the image. Using pixel world polynomial fit coefficients we calculated x - axis intersection of the left and right lanes. The midpoint of the intersections is middle of the lane. To calculate the offset of the car center with respect to middle  of the lane we substract the values calculated above as show in **curverad_and_center_dist** method. To get the value in meters we multiply calculated offset by `xm_per_pix`.  

---

## Drawing Lane 

After fitting polynomial curves for detected left and right lanes we can draw out lane approximation back onto original image together with calculated average curve radius and vehicle's offset from lane center. All the image processing pipeline is put together in **process_image** method. The result of applying **process_image** is shown below:

![Draw Lane](./output_images/draw_lane.png)

---

## Discussion and Conclusion

After getting image processing pipiline ready, I have applied it to project video. The video is included together with rest of files and is called **project_video_output.mp4**. I have spent substantial amount of time experimenting in order to get color mask values for yellow and white colors the way I wanted. Also, it took me a while to get satisfactory **src** and **dst** coordinates in order to get perspective tranform the way I liked it. That being said, the values that I have got are in no way final or most robust. When I have applied my pipeline to challenge videos, there were number of areas where lane we not calculated correctly. I think it is due to difference in lightning conditions, therefore shades of whites and yellow that my pipeline filters and input has. I will need to make adjustments with frames from challenge and extra challege video to see how can I improve my existing pipeline.  Hopefully, I can find good combination of color and gradient thresholds to get robust lane line extraction applicable in all conditions.